package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/someGet", handle)
	router.POST("somePost", handle)
	router.PUT("/somePut", handle)

	router.Any("/any", handle)

	router.Run(":8090")

}

func handle(context *gin.Context) {
	context.String(http.StatusOK, "Happy New Year 2020!")

}
