package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {

	router := gin.Default()

	v1 := router.Group("v1")
	{

		v1.POST("/login", login)
		v1.POST("/submit", submit)
		v1.POST("/read", read)
	}

	v2 := router.Group("v2")
	{
		v2.POST("/login", groupHandle)
		v2.POST("/submit", groupHandle)
		v2.POST("/read", groupHandle)
	}

	router.Run(":8080")

}

func login(content *gin.Context) {
	content.String(http.StatusOK, "login success!")
}

func submit(content *gin.Context) {
	content.String(http.StatusOK, "submit success!")
}

func read(content *gin.Context) {
	content.String(http.StatusOK, "read success!")
}

func groupHandle(content *gin.Context) {
	content.String(http.StatusOK, "v2 API")
}
