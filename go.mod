module ginEx

go 1.13

require (
	github.com/gin-contrib/sse v0.1.1-0.20190905051334-43f0f29dbd2b // indirect
	github.com/gin-gonic/gin v1.5.1-0.20191230135508-59ab588bf597
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/ugorji/go v1.1.8-0.20190812104308-42bc974514ff // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200702044944-0cc1aa72b347 // indirect
)
